package com.example.examen;

import jakarta.annotation.PostConstruct;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.springframework.stereotype.Service;



import java.util.Properties;
public class DataStreamService {
    private final Properties kafkaProperties;
    private KafkaStreams streams;

    public DataStreamService(Properties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
    }

    @PostConstruct
    public void startStream() {
        StreamsBuilder builder = new StreamsBuilder();

        KStream<Integer, Product> productStream = builder.stream("product-topic");
        KTable<Integer, ProductCostHistory> costHistoryTable = builder.table("cost-history-topic");

        productStream.join(costHistoryTable,
                        (product, costHistory) -> new JoinedProduct())
                .to("joined-product-topic");

        streams = new KafkaStreams(builder.build(), kafkaProperties);
        streams.start();
    }
}
