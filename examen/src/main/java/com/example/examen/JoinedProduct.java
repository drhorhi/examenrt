package com.example.examen;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor

public class JoinedProduct {
    private int productId;
    private String name;
    // ... other fields from Product
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private BigDecimal standardCost;
    private LocalDateTime productModifiedDate;
    private LocalDateTime costHistoryModifiedDate;
}
