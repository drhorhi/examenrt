package com.example.examen;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
@AllArgsConstructor
@Data
public class ProductCostHistory {
    private int productId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private BigDecimal standardCost;
    private LocalDateTime modifiedDate;
}
