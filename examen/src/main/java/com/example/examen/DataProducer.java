package com.example.examen;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import java.util.Properties;
public class DataProducer {
    private KafkaProducer<String, Product> producer;

    public DataProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // Additional producer configurations...
        this.producer = new KafkaProducer<>(props);
    }

    public void send(String topic, Product data) {
        producer.send(new ProducerRecord<>(topic, data));
        // You might want to handle callbacks or implement a flushing mechanism
    }
}
