package com.example.examen;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
    public class Product {
        private int productId;
        private String name;
        private String productNumber;
        private boolean makeFlag;
        private boolean finishedGoodsFlag;
        private String color;
        private int safetyStockLevel;




}