package com.example.exambatch;
import java.math.BigDecimal;
import java.time.LocalDateTime;
public record JoinedProduct(
        int productId,
        String name,
        String productNumber,
        BigDecimal standardCost,
        BigDecimal listPrice,
        LocalDateTime startDate,
        LocalDateTime endDate
) {
}
