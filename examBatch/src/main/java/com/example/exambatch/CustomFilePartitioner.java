package com.example.exambatch;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.core.io.Resource;

import java.io.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.Map;

public class CustomFilePartitioner implements Partitioner {
    private final Resource resource;
    private final int linesToSkip;
    private final int linesPerRecord;

    public CustomFilePartitioner(Resource resource, int linesToSkip, int linesPerRecord) {
        this.resource = resource;
        this.linesToSkip = linesToSkip;
        this.linesPerRecord = linesPerRecord;
    }


    @Override
    public Map<String, ExecutionContext> partition(int gridSize) {

        Map<String, ExecutionContext> result = new HashMap<>();
        int totalLines = determineTotalLines(resource);
        int targetSize = (int) Math.ceil(totalLines * 0.05);
        int numberOfSegments = (int) Math.ceil((double) totalLines / targetSize);

        for (int i = 0; i < numberOfSegments; i++) {
            ExecutionContext value = new ExecutionContext();
            value.putInt("segment", i);
            value.putInt("linesToSkip", linesToSkip + (i * targetSize));
            value.putInt("maxItemCount", targetSize);

            result.put("partition" + i, value);
        }

        return result;
    }

    private int determineTotalLines(Resource resource) {
        int lineCount = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(resource.getFile()))) {
            while (reader.readLine() != null) {
                lineCount++;
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to read the file and count lines", e);
        }
        return lineCount;
    }

}
