package com.example.exambatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamBatchApplication.class, args);
	}

}
