package com.example.exambatch;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;

import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.partition.support.PartitionStep;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.support.JdbcTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
@Configuration
public class JobConfiguration {

    @Bean
    public Job job(JobRepository jobRepository, Step step1, Step step2) {
        return new JobBuilder("databaseExtractionJob", jobRepository)
                .start(step1)
                .next(step2)
                .build();
    }

    @Bean
    public Step step1(JobRepository jobRepository, JdbcTransactionManager transactionManager) {
        return new StepBuilder("filePreparationStep", jobRepository)
                .tasklet(new FilePreparationTasklet(), transactionManager)
                .build();
    }

    @Bean
    public FlatFileItemReader<JoinedProduct> productDataReader() {
        return new FlatFileItemReaderBuilder<JoinedProduct>()
                .name("ProductDataReader")
                .resource(new FileSystemResource("resources/larger_products_data.csv"))
                .delimited()
                .names(new String[]{"productId", "name", "productNumber", "standardCost", "listPrice", "startDate", "endDate"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<>() {{
                    setTargetType(JoinedProduct.class);
                }})
                .build();
    }
    @Bean
    public Partitioner customFilePartitioner() {

        FileSystemResource fileSystemResource = new FileSystemResource("resources/larger_products_data.csv");
        int linesToSkip = 1;
        int linesPerRecord = 100;
        return new CustomFilePartitioner(fileSystemResource, linesToSkip, linesPerRecord);
    }


    @Bean
    public JdbcBatchItemWriter<JoinedProduct> productDataWriter(DataSource dataSource) {
        String sql = "INSERT INTO joined_product (product_id, name, product_number, standard_cost, list_price, start_date, end_date) VALUES (:productId, :name, :productNumber, :standardCost, :listPrice, :startDate, :endDate)";
        return new JdbcBatchItemWriterBuilder<JoinedProduct>()
                .dataSource(dataSource)
                .sql(sql)
                .beanMapped()
                .build();
    }

    @Bean
    public Step step2(JobRepository jobRepository,
                      JdbcTransactionManager transactionManager,
                      ItemReader<JoinedProduct> productDataReader,
                      ItemWriter<JoinedProduct> productDataWriter) {
        return new StepBuilder("dataUploadStep", jobRepository)
                .<JoinedProduct, JoinedProduct>chunk(100, transactionManager)
                .reader(productDataReader)
                .writer(productDataWriter)
                .faultTolerant()
                .skipLimit(10)
                .retryLimit(3)
                .build();
    }
}